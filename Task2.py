#!/usr/bin/env python3
"""Task 2 from page 56"""


def convert_int():
    """Takes string and converts it to int"""
    digit: str = input("Give me int ")
    digit: int = int(digit)
    print(digit)

def convert_float():
    """Takes string and converts it to float"""
    digit: str = input("Give me float ")
    digit: float = float(digit)
    print(digit)


convert_int()
convert_float()
