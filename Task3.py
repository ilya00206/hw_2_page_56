#!/usr/bin/env python3
"""Task 3 from page 56"""


class Flower:
    """Flower class."""
    def __init__(self, flower_name: str, color: str):
        """Ctor with flower name and color parameters."""
        self.name: str = flower_name
        self.color: str = color

    def __str__(self) -> str:
        """Returns the string representation of the object."""
        return self.name + " flower is " + self.color


rose: Flower = Flower("Rose", "red")
print(rose)
